FROM ubuntu:16.04
LABEL Description="This image is a clean build of mooncoin from it's github default branch"

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list

RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN git clone https://github.com/mooncoindev/mooncoin.git /opt/mooncoin && \
    cd /opt/mooncoin && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --without-gui --without-qrcode && \
    make && \
    cp /opt/mooncoin/src/Mooncoind /usr/local/bin/ && \
    cp /opt/mooncoin/src/Mooncoin-cli /usr/local/bin/ && \
    rm -rf /opt/mooncoin

RUN groupadd -r mooncoin && useradd -r -m -g mooncoin mooncoin
ENV MOONCOIN_DATA /data
RUN mkdir $MOONCOIN_DATA
COPY Mooncoin.conf $MOONCOIN_DATA/Mooncoin.conf
RUN chown mooncoin:mooncoin $MOONCOIN_DATA && \
    ln -s $MOONCOIN_DATA /home/mooncoin/.Mooncoin
USER mooncoin
VOLUME /data
EXPOSE 44664 44663
CMD ["/usr/local/bin/Mooncoind", "-conf=/data/Mooncoin.conf", "-server", "-txindex", "-printtoconsole"]